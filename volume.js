/////////////////////////////////////////////////////////////////////////////////////////////
//
// volume
//
//    Library for working with storage volumes.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// TODO - Add classes, types, ...
//
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var event = require("event");

function unsupported() {
    return new Promise(function (resolve, refuse) {
        refuse(new Error(Error.ERROR_UNSUPPORTED_OPERATION, "The function called is not implemented."));
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Volume Prototype
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Volume = {
    id :          null,

    err :         [],
    name :        null,
    protocol :    null,
    size :        null,
    description : null,
    type :        null,
    class :       null,
    scope :       null,
    speed :       null,
    readOnly :    null,

    speed :       null,

    open :   unsupported,
    delete : unsupported,
    exists : unsupported,
    query :  unsupported,
    close :  unsupported,

    // if implemented, needs to be overwritten
    events : new event.Emitter()
};

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Volume;
module.exports.SCOPE_LOCAL =        "volume-scope-local";
module.exports.SCOPE_REMOTE =       "volume-scope-remote";
module.exports.CLASS_PERSISTENT =   "volume-class-persistent";
module.exports.CLASS_TEMPORARY =    "volume-class-temporary";
module.exports.SPEED_UNKOWN =       null;
module.exports.SPEED_SLOW =         "volume-speed-slow";
module.exports.SPEED_REASONABLE =   "volume-speed-reasonable";
module.exports.SPEED_FAST =         "volume-speed-fast";
module.exports.SPEED_INSTANT =      "volume-speed-instant";
module.exports.STATE_LOCKED =       "volume-state-locked";
module.exports.STATE_READY =        "volume-state-ready";
module.exports.STATE_ERROR =        "volume-state-error";
module.exports.TYPE_FIXED =         "volume-type-fixed";
module.exports.TYPE_REMOVABLE =     "volume-type-removable";